'use strict';

$(document).ready(function() {

    $('.tabs a').click(function(){
        $('.tab-content').hide();
        $('.tabs a.active').removeClass('active');
        $(this).addClass('active');

        const content = $(this).attr('href');
        $(content).fadeIn(200);
        return false;
    });

    $('.tabs li:first a').click();

});
